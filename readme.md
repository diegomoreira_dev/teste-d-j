# Aplicativo Web # 

### O que foi utilizado? ###
- PHP
- Laravel 5.2
- HTML 5
- CSS 
- Jquery
- MySql

Sistema operacional - Windows 10
Servidor - WAMP


### Versionamento ### 
https://bitbucket.org/diegomoreira_dev/teste-d-j


### Como reproduzir o sistema? ###

1ª - Criar um banco de dados MySql com o nome “teste_diego”.

2ª - Baixar o conteúdo do repositório para seu servidor.

3ª - Abra o Prompt de Comando e execute os seguintes comandos:

    cd /
    cd wamp/www/teste_diego
    php artisan migrate 
    php artisan db:seed
    php artisan serve --port=8888

4ª - Abra o seu navegador e digite o endereço “localhost:8888”.

Dados de acesso
Email: **diego@gmail.com**
Senha: **1234**