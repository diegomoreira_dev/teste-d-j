<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Funcionario;
use DB;

class FuncionarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	/**
     * Exbir Funcionário por $id
     */
	public function funcionario($id){
		$funcionario = DB::table('funcionarios')->where('id', $id)->first();
        return view('funcionario/funcionario', ['funcionario' => $funcionario ]);
	}
	
	/**
     * Cadastrar Funcionário 
     */
	public function novo(Request $request){
		if ($request->isMethod('post')) {
			
			$functionarioTable = new Funcionario;
			$erro_image = "";
			
			if ( isset( $_FILES[ 'arquivo' ][ 'name' ] ) && $_FILES[ 'arquivo' ][ 'error' ] == 0 ) {
			 
			    $arquivo_tmp = $_FILES[ 'arquivo' ][ 'tmp_name' ];
			    $nome = $_FILES[ 'arquivo' ][ 'name' ];
			 
			    $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );
			 
			    $extensao = strtolower ( $extensao );
			 
			    if ( strstr ( '.jpg;.jpeg;.gif;.png', $extensao ) ) {
			        $novoNome = uniqid ( time () ) . ".".$extensao;
			 
			        $destino = 'img\\funcionarios\\'.$novoNome;
			 		
			        if ( @move_uploaded_file ( $arquivo_tmp, $destino ) ) {
			        	$functionarioTable->foto = $novoNome; // input['name'] = foto
			        
					}
			        else
			            $erro_image = 'Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita.<br />';
			    }
			    else
					$erro_image = 'Você poderá enviar apenas arquivos "*.jpg;*.jpeg;*.gif;*.png"<br />';
			}
			else{
			    $erro_image = 'Você não enviou nenhum arquivo!';
			}
			
			if($erro_image != ""){
				$request->session()->flash('erro_image', $erro_image);
			}
			
			$functionarioTable->nome = $request->input('nome'); // input['name'] = nome
			$functionarioTable->email = $request->input('email'); // input['name'] = email
			$functionarioTable->setor = $request->input('setor'); // input['name'] = setor
			$functionarioTable->cargo = $request->input('cargo'); // input['name'] = cargo

	        if($functionarioTable->save()){
	        	$request->session()->flash('success', 'Funcionário cadastrado com sucesso!');
	        }
			
			
			return redirect('/');
		}
		
		return view('funcionario/novo');
		
	}
	
	/**
     * Editar Funcionário
     */
	public function editar(Request $request, $id){
		if ($request->isMethod('post')) {
			$functionarioTable = Funcionario::find($id);
			
			$erro_image = "";
			
			if ( isset( $_FILES[ 'arquivo' ][ 'name' ] ) && $_FILES[ 'arquivo' ][ 'error' ] == 0 ) {
			    $arquivo_tmp = $_FILES[ 'arquivo' ][ 'tmp_name' ];
			    $nome = $_FILES[ 'arquivo' ][ 'name' ];
			 
			    $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );
			 
			    $extensao = strtolower ( $extensao );
			 
			    if ( strstr ( '.jpg;.jpeg;.gif;.png', $extensao ) ) {
			        $novoNome = uniqid ( time () ) . ".".$extensao;
			 
			        $destino = 'img\\funcionarios\\'.$novoNome;
			 		
			        if ( @move_uploaded_file ( $arquivo_tmp, $destino ) ) {
			        	
						$nomeAntigo = $request->input('nome_antigo'); // input['name'] = nome_antigo
			        	unlink('img\\funcionarios\\'.$nomeAntigo);
						
			        	$functionarioTable->foto = $novoNome; // input['name'] = foto
					}
			        else
			            $erro_image = 'Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita.<br />';
			    }
			    else
					$erro_image = 'Você poderá enviar apenas arquivos "*.jpg;*.jpeg;*.gif;*.png"<br />';
			}
			else{
			    $erro_image = 'Você não enviou nenhum arquivo!';
			}


			$functionarioTable->nome = $request->input('nome'); // input['name'] = nome
			$functionarioTable->email = $request->input('email'); // input['name'] = email
			$functionarioTable->setor = $request->input('setor'); // input['name'] = setor
			$functionarioTable->cargo = $request->input('cargo'); // input['name'] = cargo
			
			if($functionarioTable->save()){
	        	$request->session()->flash('success', 'Funcionário editado com sucesso!');
	        }
		}
		
		$funcionario = DB::table('funcionarios')->where('id', $id)->first();
		
       return view('funcionario/editar', ['funcionario' => $funcionario ]);
	}
	
	/**
     * Excluir Funcionário
     */
	public function excluir(Request $request, $id){
		if ($request->isMethod('post')) {
			$functionarioTable = Funcionario::find($id);
			$funcionario = DB::table('funcionarios')->where('id', $id)->first();
			
			//echo($funcionario->foto); 
			@unlink('img\\funcionarios\\'.$funcionario->foto);
			
			if($functionarioTable->delete()){
	        	$request->session()->flash('success', 'Funcionário excluído com sucesso!');
	        }
			
			return redirect('/');
		}
		
		$funcionario = DB::table('funcionarios')->where('id', $id)->first();
		
        return view('funcionario/excluir', ['funcionario' => $funcionario ]);
	}
}
