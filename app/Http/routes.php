<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/funcionario', 'HomeController@index');

Route::get('/funcionario/{id}', 'FuncionarioController@funcionario');

Route::match(['get', 'post'], '/novofuncionario/', 'FuncionarioController@novo');

Route::match(['get', 'post'], '/funcionario/editar/{id}', 'FuncionarioController@editar');

Route::match(['get', 'post'], '/funcionario/excluir/{id}', 'FuncionarioController@excluir');

