@extends('layouts.app')

@section('content')

	<div class="header-container">
		<div class="header-containerl-left">
			<h1>
				Novo Funcionário <i class="fa fa-users" aria-hidden="true"></i>
			</h1>
		</div>
		<div class="header-containerl-right">
			<a class="btn-novo-funcionario" href="{{ url('/') }}" >
				<i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar 
				</i>
			</a>	
		</div>
	</div>
	<br />
	<br />
	<hr />
	<form role="form" method="POST" action="{{ url('/novofuncionario') }}" enctype="multipart/form-data" >
		{{ csrf_field() }}
		<div class="form-item">
			<label>
				Nome
			</label>
			<input required="" type="text" name="nome"  /> <br />
		</div>
		<div class="form-item">
			<label>
				Email
			</label>
			<input required="" type="email" name="email"  /> <br />
		</div>
		<div class="form-item">
			<label>
				Setor
			</label>
			<input type="text" required="" name="setor"  /> <br />
		</div>
		<div class="form-item">
			<label>
				Cargo
			</label>
			<input type="text" name="cargo" required=""  /> <br />
		</div>
		<div class="form-item">
			<label>
				Imagem
			</label>
			<input type="file" name="arquivo" /> <br />
		</div>
		<button class="btn-submit" type="submit">
			SALVAR
		</button>
	</form>

@endsection