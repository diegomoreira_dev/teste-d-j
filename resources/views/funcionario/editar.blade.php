@extends('layouts.app')

@section('content')

	<div class="header-container">
		<div class="header-containerl-left">
			<h1>
				Editar Funcionários <i class="fa fa-users" aria-hidden="true"></i>
			</h1>
		</div>
		<div class="header-containerl-right">
			<a class="btn-novo-funcionario" href="{{ url('/') }}" >
				<i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar 
				</i>
			</a>	
		</div>
	</div>
	<br />
	<br />
	<hr />
	<form role="form" method="POST" action="{{ url('/funcionario/editar') }}/{{ $funcionario->id }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $funcionario->id }}" /> <br />
		<div class="form-item">
			<label>
				Nome
			</label>
			<input type="text" name="nome" value="{{ $funcionario->nome }}" /> <br />
		</div>
		<div class="form-item">
			<label>
				Email
			</label>
			<input type="text" name="email" value="{{ $funcionario->email }}" /> <br />
		</div>
		<div class="form-item">
			<label>
				Setor
			</label>
			<input type="text" name="setor" value="{{ $funcionario->setor }}" /> <br />
		</div>
		<div class="form-item">
			<label>
				Cargo
			</label>
			<input type="text" name="cargo" value="{{ $funcionario->cargo }}" /> <br />
		</div>
		<div class="form-item">
			<label>
				Foto
			</label>
			<input type="hidden" name="nome_antigo" value="{{ $funcionario->foto }}" />
			<input type="file" name="arquivo" class="arquivos" /> <br />
			@if($funcionario->foto == "")
				<img id="img-load" src="{{ url('/') }}/img/funcionarios/padrao-vazio.jpg" width="300px" height="auto" />
			@else
				<img id="img-load" src="{{ url('/') }}/img/funcionarios/{{ $funcionario->foto }}" width="300px" height="auto" />
			@endif
		</div>
		<button class="btn-submit" type="submit">
			SALVAR
		</button>
	</form>

@endsection