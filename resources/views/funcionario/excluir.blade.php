<div id="mask"></div>
@extends('layouts.app')

@section('content')

	<div class="header-container">
		<div class="header-containerl-left">
			<h1> Funcionário <i class="fa fa-users" aria-hidden="true"></i></h1>
		</div>
		<div class="header-containerl-right">
			<a class="btn-novo-funcionario" href="{{ url('/') }}" > <i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar
			</i> </a>
		</div>
	</div>
	<br />
	<br />
	<hr />
	<form role="form" method="POST" action="{{ url('/funcionario/excluir') }}/{{ $funcionario->id }}">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $funcionario->id }}" />
		<br />
		<div class="form-item">
			<label> Nome </label>
			<input disabled="" type="text" name="nome" value="{{ $funcionario->nome }}" />
			<br />
		</div>
		<div class="form-item">
			<label> Email </label>
			<input disabled="" type="text" name="email" value="{{ $funcionario->email }}" />
			<br />
		</div>
		<div class="form-item">
			<label> Setor </label>
			<input disabled="" type="text" name="setor" value="{{ $funcionario->setor }}" />
			<br />
		</div>
		<div class="form-item">
			<label> Cargo </label>
			<input type="text" disabled="" name="cargo" value="{{ $funcionario->cargo }}" />
			<br />
		</div>
		<div class="form-item">
			<label>
				Foto
			</label>
			@if($funcionario->foto == "")
				<img id="img-load" src="{{ url('/') }}/img/funcionarios/padrao-vazio.jpg" width="300px" height="auto" />
			@else
				<img id="img-load" src="{{ url('/') }}/img/funcionarios/{{ $funcionario->foto }}" width="300px" height="auto" />
			@endif
			<br />
			<br />
		</div>
		<a class="btn-submit" href="#dialog" name="modal">
			EXCLUIR
		</a>
	</form>
	

	<div id="boxes">

		<div id="dialog" class="window">
			<b>Confirme</b>
			<a href="#" class="close">Fechar [X]</a>
			<br />
			<form role="form" method="POST" action="{{ url('/funcionario/excluir') }}/{{ $funcionario->id }}">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="{{ $funcionario->id }}" />
				<br />
				Tem certeza que desaja excluir esse funcionário?
				<br />
				<br />
				<button class="btn-confirme" type="submit">
					Sim
				</button>
				<a class="btn-n-confirme close" href="#">
					Não
				</a>
			</form>
		</div>

	</div>

<script type="text/javascript">
	$(document).ready(function() {

		
	}); 
</script>

@endsection

