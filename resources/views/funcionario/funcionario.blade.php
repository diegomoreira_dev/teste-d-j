@extends('layouts.app')

@section('content')

	<div class="header-container">
		<div class="header-containerl-left">
			<h1>
				Funcionário <i class="fa fa-users" aria-hidden="true"></i>
			</h1>
		</div>
		<div class="header-containerl-right">
			<a class="btn-novo-funcionario" href="{{ url('/') }}" >
				<i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar 
				</i>
			</a>
			&nbsp
			<a class="btn-novo-funcionario" href="{{ url('/funcionario/editar/') }}/{{ $funcionario->id }}" >
				<i class="fa fa-pencil" aria-hidden="true"></i> Editar 
				</i>
			</a>
			&nbsp
			<a class="btn-novo-funcionario" href="{{ url('/funcionario/excluir/') }}/{{ $funcionario->id }}" >
				<i class="fa fa-trash" aria-hidden="true"></i> Excluir
				</i>
			</a>	
		</div>
	</div>
	<br />
	<br />
	<hr />
	<form>
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $funcionario->id }}" /> <br />
		<div class="form-item">
			<label>
				Nome
			</label>
			<input disabled="" type="text" name="nome" value="{{ $funcionario->nome }}" /> <br />
		</div>
		<div class="form-item">
			<label>
				Email
			</label>
			<input disabled="" type="text" name="email" value="{{ $funcionario->email }}" /> <br />
		</div>
		<div class="form-item">
			<label>
				Setor
			</label>
			<input disabled="" type="text" name="setor" value="{{ $funcionario->setor }}" /> <br />
		</div>
		<div class="form-item">
			<label>
				Cargo
			</label>
			<input type="text" disabled="" name="cargo" value="{{ $funcionario->cargo }}" /> <br />
		</div>
		<div class="form-item">
			<label>
				Foto
			</label>
			@if($funcionario->foto == "")
				<img src="{{ url('/') }}/img/funcionarios/padrao-vazio.jpg" width="300px" height="auto" />
			@else
				<img src="{{ url('/') }}/img/funcionarios/{{ $funcionario->foto }}" width="300px" height="auto" />
			@endif
		<div>
	</form>

@endsection