@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ URL::asset('css/jquery.dataTables.min.css') }}">

<script src="{{ URL::asset('js/jquery-1.12.0.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}" type="text/javascript"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">

	<div class="header-container">
		<div class="header-containerl-left">
			<h1>
				Funcionários <i class="fa fa-users" aria-hidden="true"></i>
			</h1>
		</div>
		<div class="header-containerl-right">
			<a class="btn-novo-funcionario" href="{{ url('/novofuncionario') }}" >
				Novo funcionario <i class="fa fa-plus-circle" aria-hidden="true">
				</i>
			</a>	
		</div>
	</div>
	
	<div>
		<br />
		<br />
		<hr />
		<table id="tblFuncionarios" class="display" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Email</th>
					<th>Setor</th>
					<th>Cargo</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Nome</th>
					<th>Email</th>
					<th>Setor</th>
					<th>Cargo</th>
					<th>Ações</th>
				</tr>
			</tfoot>
			<tbody>
				@foreach ($funcionarios as $funcionario)
				<tr>
					<td>{{ $funcionario->nome }}</td>
					<td>{{ $funcionario->email }}</td>
					<td>{{ $funcionario->setor }}</td>
					<td>{{ $funcionario->cargo }}</td>
					<td>
						<a class="btn-tbl btn-tbl-vis" href="{{ url('/funcionario') }}/{{ $funcionario->id }}" ><i class="fa fa-search" aria-hidden="true"></i></a>
						<a class="btn-tbl btn-tbl-edit" href="{{ url('/funcionario/editar/') }}/{{ $funcionario->id }}" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a class="btn-tbl btn-tbl-excluir" href="{{ url('/funcionario/excluir/') }}/{{ $funcionario->id }}" ><i class="fa fa-trash" aria-hidden="true"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
<script>
	$(document).ready(function() {
		$('#tblFuncionarios').DataTable({
			"order" : [['0', "asc"]],
			 "oLanguage": {
			    "sProcessing": "Aguarde enquanto os dados são carregados ...",
			    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
			    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
			    "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
			    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
			    "sInfoFiltered": "",
			    "sSearch": "Procurar",
			    "oPaginate": {
			       "sFirst":    "Primeiro",
			       "sPrevious": "Anterior",
			       "sNext":     "Próximo",
			       "sLast":     "Último"
			    }
			 }                              
		});
	}); 
</script>

@endsection

