<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
	
	<meta name="author" content="Diego Moreira">
	<meta name="description" content="Tela de login (Teste - Diego Moreira)">
	
	<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
	
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.dataTables.min.css') }}">

	<script src="{{ URL::asset('js/jquery-1.12.0.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
</head>
<body>
	
	<header>

		@if(Session::has('success'))
			<div class="alert-success">
				<i class="fa fa-thumbs-up" aria-hidden="true"></i> {{Session::get('success')}}
			</div>
		 @endif
		 
		 @if(Session::has('erro_image'))
			<div class="alert-erro">
				<i class="fa fa-thumbs-down" aria-hidden="true"></i> {{Session::get('erro_image')}}
			</div>
		 @endif
		 
		<div class="menu-top">
			<div class="logo-header">
				<a class="btn-header" href="{{ url('/') }}">Teste Diego</a>
			</div>
			<div class="logout-menu">
				<a class="btn-logout" href="{{ url('/logout') }}">Sair <i class="fa fa-sign-out" aria-hidden="true"></i></a>
			</div>
		</div>
		<div class="menu-left">
			<a class="btn-menu-left" href="{{ url('/') }}"> Funcionários <i class="fa fa-users" aria-hidden="true"></i></a>
		</div>
	</header>
	<div class="display-container">
   		@yield('content')
	</div>
	<footer>
		<div class="div-footer">
			Desenvolvido por Diego Moreira
		</div>
	</footer>
	
	<script src="{{ URL::asset('js/app.js') }}" type="text/javascript"></script>
</body>
</html>
