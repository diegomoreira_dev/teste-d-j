<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">

		<title>Login</title>
		<meta name="author" content="Diego Moreira">
		<meta name="description" content="Tela de login (Teste - Diego Moreira)">
		
		<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		
		
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
		
	</head>

	<body>
		<div class="login-div">
			<div class="panel-login">
				<h1>
					<i class="fa fa-lock" aria-hidden="true"></i> Login
				</h1>
                <form class="form-login" role="form" method="POST" action="{{ url('/login') }}">
					<hr />
                    {!! csrf_field() !!}
					
					@if ($errors->has('email'))
                        <div class="erro-email">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $errors->first('email') }}
                        </div>
                    @endif
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-md-6">
                            <input required="" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">

                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                        <div class="col-md-6">
                            <input type="password" required="" class="form-control" name="password" placeholder="Senha">

                        </div>
                    </div>
					<hr />
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit">
                                <i class="fa fa-btn fa-sign-in"></i> ENTRAR
                            </button>
                        </div>
                    </div>
                </form>
            </div>
		</div>
	</body>
</html>
