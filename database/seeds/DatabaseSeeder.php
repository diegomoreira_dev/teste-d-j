<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Diego',
            'email' => 'diego@gmail.com',
            'password' => bcrypt('1234'),
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Pedro',
            'email' => 'pedro@dj.emp.br',
            'setor' => 'TI',
            'cargo' => 'Gerente',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'João',
            'email' => 'joao@dj.emp.br',
            'setor' => 'TI',
            'cargo' => 'Programador',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Flavio',
            'email' => 'flavio@dj.emp.br',
            'setor' => 'TI',
            'cargo' => 'Estagiário',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Maria',
            'email' => 'maria@dj.emp.br',
            'setor' => 'Administrativo',
            'cargo' => 'Secretária',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Amanda',
            'email' => 'amanda@dj.emp.br',
            'setor' => 'Administrativo',
            'cargo' => 'Estagiária',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Manoel',
            'email' => 'manoel@dj.emp.br',
            'setor' => 'Financeiro',
            'cargo' => 'Contador',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Laura',
            'email' => 'laura@dj.emp.br',
            'setor' => 'RH',
            'cargo' => 'Psicóloga',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Debora',
            'email' => 'debora@dj.emp.br',
            'setor' => 'RH',
            'cargo' => 'Gerente',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Bruno',
            'email' => 'bruno@dj.emp.br',
            'setor' => 'Diretoria',
            'cargo' => 'CEO',
            'foto' => '',
        ]);
		
		DB::table('funcionarios')->insert([
            'nome' => 'Rodrigo',
            'email' => 'rodrigo@dj.emp.br',
            'setor' => 'Diretoria',
            'cargo' => 'CFO',
            'foto' => '',
        ]);
		
    }
}



